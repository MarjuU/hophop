//searchbutton redirect
$(function () {
    $('#btnSearch').click(function () {
        var searchTerm = encodeURIComponent($('#txtSearch').val());
        var dates = encodeURIComponent($('#dateSearch').val());
        var guests = encodeURIComponent($('#spnSelectedGuest').val());
        var url = 'SearchResults.html?searchTerm=' + searchTerm +
            '&dates=' + dates +
            '&guests=' + guests;
        window.location.href = url;
    })
});

$(document).ready(function () {
    // Activate Carousel
    $("#myCarousel").carousel({interval: 3000, pause: "hover"});

    //To initialise the datepicker.
    $('.datepicker').datepicker({
        clearBtn: true,
        autoclose: true,
        format: 'mm-dd-yyyy',
        todayHighlight: true,
        multidate: 3,
        startDate: new Date(),
    });

    //dropdown funcionality
    $('.dropdown-menu a').click(function () {
        var number = ($(this).text().split(' '))[0];
        $('#spnSelectedGuest').text('Number of guests - ' + $(this).text()).val(number);
    });
});

function openNav() {
    debugger;
    document.getElementById("mySidenav").style.width = "250px";
    document.body.style.backgroundColor = "rgba(0,0,0,.5);";
}

function closeNav() {
    debugger;
    document.getElementById("mySidenav").style.width = "0";
    document.body.style.backgroundColor = "white";
}
