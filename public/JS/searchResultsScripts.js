var urlParams = new URLSearchParams(window.location.search);

console.log(urlParams.has('post')); // true
console.log(urlParams.get('action')); // "edit"
console.log(urlParams.getAll('action')); // ["edit"]
console.log(urlParams.toString()); // "?post=1234&action=edit"
console.log(urlParams.append('active', '1')); // "?post=1234&action=edit&active=1"

var keys = urlParams.keys();
for (key of keys) {
    console.log(key);
}

var entries = urlParams.entries();
for (pair of entries) {
    console.log(pair[0], pair[1])
}