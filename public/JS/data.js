
var data = [
  { label: "New Delhi", category: "Cities" },
  { label: "Bangalore", category: "Cities" },
  { label: "Tallinn", category: "Cities" },
  { label: "Tartu", category: "Cities" },
  { label: "Toronto", category: "Cities" },
  { label: "Amsterdam", category: "Cities" },
  { label: "Paris", category: "Cities" },
  { label: "Helsinki", category: "Cities" },
  { label: "Riga", category: "Cities" },
  { label: "Vilnius", category: "Cities" },
  { label: "Sarab", category: "Hosts" },
  { label: "Nick", category: "Hosts" },
  { label: "Marju", category: "Hosts" },
  { label: "Triin", category: "Hosts" },
  { label: "Lara", category: "Hosts" },
];