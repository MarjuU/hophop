$(function () {
    $.widget("custom.catcomplete", $.ui.autocomplete, {
        _create: function () {
            this._super();
            this.widget().menu("option", "items", "> :not(.ui-autocomplete-category)");
        },
        _renderMenu: function (ul, items) {
            var that = this,
                currentCategory = "";
            $.each(items, function (index, item) {
                var li;
                if (item.category !== currentCategory) {
                    ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
                    currentCategory = item.category;
                }
                li = that._renderItemData(ul, item);
                if (item.category) {
                    li.attr("aria-label", item.category + " : " + item.label);
                }
            });
        },
        classes: {
            "ui-menu-item": "test"
        }
    });

 var autocomplete = $( "#txtSearch" ).catcomplete({
    delay: 0,
    source: data,
    classes: {
      "ui-menu-item": "test"
    }
  }).catcomplete("widget").addClass("autocomplete-menu");
} );